EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "Engineering Support Platform System wiring"
Date "2022-01-03"
Rev "A"
Comp "Woods Hole Oceanographic Inst"
Comment1 "AOPE OOI"
Comment2 "266 Woods Hole Road"
Comment3 "Woods Hole, MA 02543"
Comment4 "bkelly@whoi.edu"
$EndDescr
Text GLabel 3300 2600 0    50   Input ~ 0
CPM_DCL7_TX+_C39
Text GLabel 3300 2700 0    50   Input ~ 0
CPM_DCL7_TX-_A39
Text GLabel 3300 2800 0    50   Input ~ 0
CPM_DCL7_RX+_D39
Text GLabel 3300 2900 0    50   Input ~ 0
CPM_DCL7_RX-_B39
Text GLabel 3300 3000 0    50   Input ~ 0
CPM_DCL7_PPS+_D38
Text GLabel 3300 3100 0    50   Input ~ 0
CPM_DCL7_PPS-_C38
Text GLabel 3300 3200 0    50   Input ~ 0
CPM_DCL7_GPS+_B38
Text GLabel 3300 3300 0    50   Input ~ 0
CPM_DCL7_GPS-_A38
Text GLabel 3300 3400 0    50   Input ~ 0
CPM_DCL7_ENA_D37
Text GLabel 3300 3500 0    50   Input ~ 0
CPM_DCL7_ENA_RTN_C37
Text GLabel 3300 3600 0    50   Input ~ 0
CPM_DCL7_RTN_A_B37
Text GLabel 3425 3600 2    50   Input ~ 0
DCL_C46
Text GLabel 3425 3500 2    50   Input ~ 0
DCL_A44
Text GLabel 3425 3400 2    50   Input ~ 0
DCL_B44
Text GLabel 3425 3300 2    50   Input ~ 0
DCL_A48
Text GLabel 3425 3200 2    50   Input ~ 0
DCL_B48
Text GLabel 3425 3100 2    50   Input ~ 0
DCL_C47
Text GLabel 3425 3000 2    50   Input ~ 0
DCL_D47
Text GLabel 3425 2900 2    50   Input ~ 0
DCL_B45
Text GLabel 3425 2800 2    50   Input ~ 0
DCL_B46
Text GLabel 3425 2700 2    50   Input ~ 0
DCL_A45
Text GLabel 3425 2600 2    50   Input ~ 0
DCL_A46
Wire Wire Line
	3425 2600 3300 2600
Wire Wire Line
	3300 2700 3425 2700
Wire Wire Line
	3425 2800 3300 2800
Wire Wire Line
	3300 2900 3425 2900
Wire Wire Line
	3425 3000 3300 3000
Wire Wire Line
	3300 3100 3425 3100
Wire Wire Line
	3425 3200 3300 3200
Wire Wire Line
	3300 3300 3425 3300
Wire Wire Line
	3425 3400 3300 3400
Wire Wire Line
	3300 3500 3425 3500
Wire Wire Line
	3425 3600 3300 3600
Text Notes 2625 2500 0    50   ~ 0
CPM TO DCL7 CONNECTIONS
Text GLabel 7275 3575 2    50   Input ~ 0
CPM_D16_SBD_0V
Text GLabel 7275 3475 2    50   Input ~ 0
CPM_D18_SBD_5V
Text GLabel 7275 3150 2    50   Input ~ 0
CPM_C48_GPS_4V
Text GLabel 7275 3250 2    50   Input ~ 0
CPM_C46_GPS_0V
Text GLabel 7275 2775 2    50   Input ~ 0
CPM_C40_ISU_12V
Text GLabel 7275 2875 2    50   Input ~ 0
CPM_C41_ISU_0V
Text GLabel 7275 2425 2    50   Input ~ 0
CPM_D45_RFM_12V
Text GLabel 7275 2525 2    50   Input ~ 0
CPM_D44_RFM_0V
Text GLabel 7275 3950 2    50   Input ~ 0
J4-1_CPM_0V
Text GLabel 7275 3850 2    50   Input ~ 0
J4-2_CPM_24V
Text GLabel 7275 4375 2    50   Input ~ 0
J5-1_DCL_0V
Text GLabel 7275 4275 2    50   Input ~ 0
J5-2_DCL_24V
Text GLabel 7125 3475 0    50   Input ~ 0
SBD_BLU
Text GLabel 7125 3575 0    50   Input ~ 0
SBD_BLK
Text GLabel 7150 2775 0    50   Input ~ 0
ISU_WHT
Text GLabel 7150 2875 0    50   Input ~ 0
ISU_BLK
Text GLabel 7150 3150 0    50   Input ~ 0
GPS_GRN
Text GLabel 7150 3250 0    50   Input ~ 0
GPS_BLK
Text GLabel 7150 2425 0    50   Input ~ 0
RFM_YEL
Text GLabel 7150 2525 0    50   Input ~ 0
RFM_BLK
Text GLabel 2875 3850 0    50   Input ~ 0
24V_RED
Text GLabel 2875 3950 0    50   Input ~ 0
24V_BLK
Text GLabel 3300 3950 2    50   Input ~ 0
METER_18AWG_BLK
Text GLabel 4875 3950 0    50   Input ~ 0
METER_18AWG_RED
Wire Wire Line
	4875 3950 5525 3950
Wire Wire Line
	5525 3950 7275 3950
Wire Wire Line
	7275 4375 5525 4375
Wire Wire Line
	5525 4375 5525 3950
Connection ~ 5525 3950
$Comp
L Switch:SW_DPST SW?
U 1 1 61E1242E
P 4275 3675
F 0 "SW?" H 4275 4000 50  0000 C CNN
F 1 "SW_DPST" H 4275 3909 50  0000 C CNN
F 2 "" H 4275 3675 50  0001 C CNN
F 3 "~" H 4275 3675 50  0001 C CNN
	1    4275 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	4075 3850 4075 3775
Wire Wire Line
	2875 3850 4075 3850
Connection ~ 4075 3775
Wire Wire Line
	4075 3775 4075 3575
Wire Wire Line
	7275 4275 5425 4275
Wire Wire Line
	5425 4275 5425 3775
Wire Wire Line
	5425 3775 4475 3775
Wire Wire Line
	7275 3850 5525 3850
Wire Wire Line
	5525 3850 5525 3575
Wire Wire Line
	5525 3575 4475 3575
Text GLabel 5350 3375 0    50   Input ~ 0
METER_22AWG_RED
Text GLabel 3950 4075 0    50   Input ~ 0
METER_22AWG_BLK-NOT_CONNECTED
Wire Wire Line
	2875 3950 3300 3950
Text GLabel 5350 3250 0    50   Input ~ 0
METER_22AWG_YEL
Wire Wire Line
	5525 3575 5525 3375
Wire Wire Line
	5525 3375 5350 3375
Connection ~ 5525 3575
Wire Wire Line
	5350 3250 5525 3250
Wire Wire Line
	5525 3250 5525 3375
Connection ~ 5525 3375
Wire Wire Line
	7150 2425 7275 2425
Wire Wire Line
	7275 2525 7150 2525
Wire Wire Line
	7150 2775 7275 2775
Wire Wire Line
	7275 2875 7150 2875
Wire Wire Line
	7150 3150 7275 3150
Wire Wire Line
	7275 3250 7150 3250
Wire Wire Line
	7275 3475 7125 3475
Wire Wire Line
	7275 3575 7125 3575
Text GLabel 2550 5150 0    50   Input ~ 0
RED
Text GLabel 2550 5050 0    50   Input ~ 0
ORA
Text GLabel 2550 4950 0    50   Input ~ 0
WHT
Text GLabel 2550 4850 0    50   Input ~ 0
BLK
Text GLabel 2550 4750 0    50   Input ~ 0
YEL
Text Notes 2325 5275 1    50   ~ 0
USB-RS422-WE
Text GLabel 2850 5150 2    50   Input ~ 0
A45
Text GLabel 2850 5050 2    50   Input ~ 0
A46
Text GLabel 2850 4950 2    50   Input ~ 0
B45
Text GLabel 2850 4850 2    50   Input ~ 0
C46
Text GLabel 2850 4750 2    50   Input ~ 0
B46
Text Notes 3125 4600 3    50   ~ 0
DCL 192 pin Conn\n
Wire Wire Line
	2850 4750 2550 4750
Wire Wire Line
	2550 4850 2850 4850
Wire Wire Line
	2850 4950 2550 4950
Wire Wire Line
	2550 5050 2850 5050
Wire Wire Line
	2850 5150 2550 5150
$EndSCHEMATC
